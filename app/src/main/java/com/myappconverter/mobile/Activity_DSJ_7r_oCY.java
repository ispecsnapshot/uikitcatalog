package com.myappconverter.mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.myappconverter.mobile.adapter.BaseAdapter_dyj_uS_k8j;


public class Activity_DSJ_7r_oCY extends AppCompatActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsj_7r_ocy);
        View view = findViewById(android.R.id.content);
        
		ListView listView_dyj_uS_k8j = (ListView) view.findViewById(R.id.dyj_uS_k8j);
		listView_dyj_uS_k8j.setAdapter(new BaseAdapter_dyj_uS_k8j());

		// Swipe navigation
		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_Activity_LZ1_eW_n5K);
		View viewSwipe = Utils.addSwipeView(relativeLayout, this);
		viewSwipe.setOnTouchListener(new OnSwipeTouchListener(this,viewSwipe));

		View btnLeft = findViewById(R.id.btn_swipe_left);
	    btnLeft.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_zcI_oN_J3j.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_right, R.anim.translate_out_right);
	        finish();
	      }
	    });

	    View btnRight = findViewById(R.id.btn_swipe_right);
	    btnRight.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_pUP_b0_VMf.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_left, R.anim.translate_out_left);
	        finish();
	      }
	    });

		TextView titleBackBarButton = (TextView) view.findViewById(R.id.nav_item_back_title);
        if(titleBackBarButton!=null) {
            titleBackBarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }


	}

}