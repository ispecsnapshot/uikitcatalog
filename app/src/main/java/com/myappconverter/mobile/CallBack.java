package com.myappconverter.mobile;

/**
 * Created by Ahmed on 28/10/2016.
 */
public interface CallBack {

    void perform(int position);
}
