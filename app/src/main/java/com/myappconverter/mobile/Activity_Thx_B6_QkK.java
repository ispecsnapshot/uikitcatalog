package com.myappconverter.mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.myappconverter.mobile.adapter.BaseAdapter_QvM_3S_GbD;


public class Activity_Thx_B6_QkK extends AppCompatActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thx_b6_qkk);
        View view = findViewById(android.R.id.content);
        
		ListView listView_QvM_3S_GbD = (ListView) view.findViewById(R.id.QvM_3S_GbD);
		listView_QvM_3S_GbD.setAdapter(new BaseAdapter_QvM_3S_GbD());

		// Swipe navigation
		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_Activity_EW7_qW_Fsk);
		View viewSwipe = Utils.addSwipeView(relativeLayout, this);
		viewSwipe.setOnTouchListener(new OnSwipeTouchListener(this,viewSwipe));

		View btnLeft = findViewById(R.id.btn_swipe_left);
	    btnLeft.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_aTT_8g_UdL.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_right, R.anim.translate_out_right);
	        finish();
	      }
	    });

	    View btnRight = findViewById(R.id.btn_swipe_right);
	    btnRight.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_Mhd_Bs_sIn.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_left, R.anim.translate_out_left);
	        finish();
	      }
	    });

		
	}

}