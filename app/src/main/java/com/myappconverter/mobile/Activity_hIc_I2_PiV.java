package com.myappconverter.mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class Activity_hIc_I2_PiV extends AppCompatActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hic_i2_piv);
        View view = findViewById(android.R.id.content);
        
		Spinner spinner_cg8_rc_3S1 = (Spinner) view.findViewById(R.id.cg8_rc_3S1);
		List<String> listspinner_cg8_rc_3S1 = new ArrayList<String>();
		listspinner_cg8_rc_3S1.add("choose a value");
		listspinner_cg8_rc_3S1.add("value 1");
		listspinner_cg8_rc_3S1.add("value 2");
		listspinner_cg8_rc_3S1.add("value 3");
		ArrayAdapter<String> arrayAdapterspinner_cg8_rc_3S1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listspinner_cg8_rc_3S1);
		arrayAdapterspinner_cg8_rc_3S1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_cg8_rc_3S1.setAdapter(arrayAdapterspinner_cg8_rc_3S1);

		// Swipe navigation
		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_Activity_gkd_fd_9L5);
		View viewSwipe = Utils.addSwipeView(relativeLayout, this);
		viewSwipe.setOnTouchListener(new OnSwipeTouchListener(this,viewSwipe));

		View btnLeft = findViewById(R.id.btn_swipe_left);
	    btnLeft.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_ZSF_lM_gQa.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_right, R.anim.translate_out_right);
	        finish();
	      }
	    });

	    View btnRight = findViewById(R.id.btn_swipe_right);
	    btnRight.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_aY0_FD_8DD.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_left, R.anim.translate_out_left);
	        finish();
	      }
	    });

		TextView titleBackBarButton = (TextView) view.findViewById(R.id.nav_item_back_title);
        if(titleBackBarButton!=null) {
            titleBackBarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }


	}

}