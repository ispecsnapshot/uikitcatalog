package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_LTn_gr_hZ8 extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_c6p_xx_oipcell_pj0_1u_e34, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_c6p_xx_oipcell_aex_df_b63, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_c6p_xx_oipcell_bdv_xk_qed, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
