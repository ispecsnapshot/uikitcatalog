package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_gDs_OX_aHt extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_zp1_vp_dmj_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_kep_mb_jnt, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_yhv_dt_yah_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_h90_oi_fmq, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_uaa_gd_ork_header, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_zoj_h2_9xs, null);
                break;

				case 6:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_g6u_rt_rxy_header, null);
                break;

				case 7:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_fho_qu_dbd, null);
                break;

				case 8:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_orn_0a_yt5_header, null);
                break;

				case 9:
                convertView = layoutInflater.inflate(R.layout.activity_gl0_hm_ewgcell_anb_xx_idg, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
