package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_d2N_kr_XWw extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_v21_vf_flhcell_1pr_u6_zci_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_v21_vf_flhcell_td3_jx_xej, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_v21_vf_flhcell_ijh_sg_gim, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_v21_vf_flhcell_sfu_ko_rsc_header, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_v21_vf_flhcell_xov_qp_q7h, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
