package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_dyj_uS_k8j extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_2js_a6_933_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_tt6_se_2xg, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_ugz_rf_ygf_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_byo_kf_g5d, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_9am_ev_vhx_header, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_ear_is_gih, null);
                break;

				case 6:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_8fd_bp_7ej_header, null);
                break;

				case 7:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_mgp_n1_ixx, null);
                break;

				case 8:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_l4j_0i_akf_header, null);
                break;

				case 9:
                convertView = layoutInflater.inflate(R.layout.activity_dsj_7r_ocycell_p2q_cs_yay, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
