package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_vxe_YC_yBi extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_ay0_fd_8ddcell_eue_e8_yfw_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_ay0_fd_8ddcell_kmi_xt_ltg, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_ay0_fd_8ddcell_bkc_ks_ihm_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_ay0_fd_8ddcell_8gd_go_6hh, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_ay0_fd_8ddcell_xt2_f1_qkq_header, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_ay0_fd_8ddcell_e4s_qv_cwn, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
