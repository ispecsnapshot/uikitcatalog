package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_id_8xG_xW_Mvc extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_laz_6n_zhecell_gm5_i7_clr_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_laz_6n_zhecell_3ns_i6_0fn, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_laz_6n_zhecell_qhz_ob_bus_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_laz_6n_zhecell_xbb_zz_6xb, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_laz_6n_zhecell_61k_iv_rit_header, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_laz_6n_zhecell_4ob_jl_pxb, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
