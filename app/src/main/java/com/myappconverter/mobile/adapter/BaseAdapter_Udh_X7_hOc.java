package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_Udh_X7_hOc extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_yvc_vn_msdcell_9eg_px_muo_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_yvc_vn_msdcell_yn2_ta_5b4, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_yvc_vn_msdcell_fua_1k_3rv_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_yvc_vn_msdcell_z0b_yv_ic1, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
