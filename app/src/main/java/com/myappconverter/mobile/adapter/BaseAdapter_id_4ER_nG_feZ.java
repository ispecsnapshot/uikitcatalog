package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_id_4ER_nG_feZ extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_zci_on_j3jcell_a1x_fu_xoh_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_zci_on_j3jcell_kaw_np_hdh, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_zci_on_j3jcell_cbk_hr_hjd_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_zci_on_j3jcell_yon_1u_p9a, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
