package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_iro_ev_0xV extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 9;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_jaz_ik_3it_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_zpj_of_wnx, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_hkt_sx_alg, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_9ff_wl_xvr, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_jwj_wz_ngx, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_q1b_zd_fgr, null);
                break;

				case 6:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_8a1_2z_sye_header, null);
                break;

				case 7:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_syv_lc_emb, null);
                break;

				case 8:
                convertView = layoutInflater.inflate(R.layout.activity_va4_ge_jqncell_btn_4f_7qb, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
