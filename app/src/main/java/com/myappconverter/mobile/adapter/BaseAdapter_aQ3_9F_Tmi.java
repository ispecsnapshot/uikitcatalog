package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_aQ3_9F_Tmi extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_ob2_pp_acjcell_ocn_zs_upr_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_ob2_pp_acjcell_mfv_3k_2qs, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_ob2_pp_acjcell_ypy_xd_oxi_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_ob2_pp_acjcell_zsy_ff_fyo, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_ob2_pp_acjcell_v11_6g_9xu_header, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_ob2_pp_acjcell_g5u_bn_ech, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
