package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_fLQ_5f_tUh extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
    @Override
    public int getCount() {
        return 8;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_hca_pz_1xg_header, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_8eu_yt_dxb, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_ylt_fk_xee_header, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_3qp_zk_6mx, null);
                break;

				case 4:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_lbr_nb_zpu_header, null);
                break;

				case 5:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_p73_fd_xoq, null);
                break;

				case 6:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_flv_0z_gfn_header, null);
                break;

				case 7:
                convertView = layoutInflater.inflate(R.layout.activity_eil_ie_nn6cell_htb_lv_lza, null);
                break;

			}

		convertView.setTag(position);
                return convertView;
    }
}
